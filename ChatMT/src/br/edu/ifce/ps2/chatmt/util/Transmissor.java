package br.edu.ifce.ps2.chatmt.util;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

import javax.swing.JOptionPane;

public class Transmissor implements Runnable {
	private Socket canal;
	@Override
	public void run() {
		try {
			ObjectOutputStream saida = 
					new ObjectOutputStream(
						canal.getOutputStream());
			while (true) {
				String msg =
					JOptionPane.showInputDialog(
							"Sua mensagem:");
				saida.writeUTF(msg);
				saida.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public Socket getCanal() {
		return canal;
	}
	public void setCanal(Socket canal) {
		this.canal = canal;
	}

}
