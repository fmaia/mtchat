package br.edu.ifce.ps2.chatmt.util;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Cliente {

	public static void main(String[] args) {
		Socket cliente;
		InetAddress serverIP;
		try {
			serverIP = InetAddress.getByAddress(new byte[] {127,0,0,1});
			cliente = new Socket(serverIP, 12347);
			Transmissor t = new Transmissor();
			t.setCanal(cliente);
			Thread tt = new Thread(t);
			tt.start();
			
			Receptor r = new Receptor();
			r.setCanal(cliente);
			Thread tr = new Thread(r);
			tr.start();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
